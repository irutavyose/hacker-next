import Link from "next/link"
import Head from "next/head";
import Router from "next/router"

const Layout = ({ title, children, description, backButtom }) => (
    <div>
        <Head>
            <title>{title}</title>
            <meta name="description" content={description} />
        </Head>
        <div className="container">
            <nav>
                {backButtom && <span onClick={() => Router.back()} className="back-buttom">&#x2b05;</span>}
                <Link href="/">
                    <a>
                        <span className="main-title">Hacker Next</span>
                    </a>
                </Link>
            </nav>
            {children}
        </div>
        <style jsx>
            {`
               .container{
                   max-width: 800px;
                   margin: 0 auto;
                   backgound: #f6f6ef;
               } 
               nav{
                   background: #f60;
                   padding: 1em;
               }
               nav > * {
                   display: inline-bloc;
                   color: black;
               }
               nav a {
                   text-decoration: none;
               }
               nav .main-title {
                   font-weight: bold;
               }
               nav .back-buttom{
                   font-size: 0.9rem;
                   padding-right: 1em;
                   cursor: pointer;
               }

            `}
        </style>
        <style global jsx>
            {`
                body{
                    backgound: white;
                    font-family: Veranda, Geneva, sans-serif;
                }
            `}
        </style>
    </div>
)
export default Layout;